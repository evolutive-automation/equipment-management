<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\EquipmentType;

class EquipmentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [];

        array_push($data, [
            'name' => '撿料車'
        ]);

        array_push($data, [
            'name' => '包裝站'
        ]);

        array_push($data, [
            'name' => '印刷機'
        ]);

        foreach ($data as $payload) {
            $model = EquipmentType::firstOrCreate(['name' => $payload['name']]);
            $model->update($payload);
        }
    }
}
