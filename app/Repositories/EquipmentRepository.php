<?php

namespace App\Repositories;

use App\Models\Equipment;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class EquipmentRepository extends BaseRepository
{
    protected $model = Equipment::class;

    public function getAllEquipment()
    {
        return $this->model::select('*')
            ->get();
    }
}
