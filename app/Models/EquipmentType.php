<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EquipmentType extends Model
{
    use HasFactory;

    protected $tale = 'equipment_types';

    protected $fillable = [
        'name'
    ];

    public function Equipment()
    {
        return $this->hasMany(Equipment::class, 'name', 'type');
    }
}
