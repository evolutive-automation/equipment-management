<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MaintenanceLog extends Model
{
    use HasFactory;

    protected $tale = 'maintenance_logs';

    protected $fillable = [
        'name',
        'equipment_id',
        'status',
        'estimated_complete_at'
    ];
}
