<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Equipment extends Model
{
    use HasFactory;

    protected $table = 'equipments';

    protected $fillable = [
        'name',
        'type',
        'status',
        'custodian'
    ];

    public function EquipmentType()
    {
        return $this->belongsTo(EquipmentType::class, 'type', 'name');
    }
}
