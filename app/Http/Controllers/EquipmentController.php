<?php

namespace App\Http\Controllers;

use App\Services\CreateEquipmentService;
use App\Services\DeleteEquipmentService;
use App\Services\UpdateEquipmentService;
use App\Services\GetAllEquipmentService;
use Illuminate\Http\Request;

class EquipmentController extends Controller
{
    public function createEquipment(Request $request)
    {
        return app(CreateEquipmentService::class)
            ->setPayload($request->all())
            ->exec();
    }

    public function deleteEquipment(Request $request)
    {
        return app(DeleteEquipmentService::class)
            ->setPayload($request->all())
            ->exec();
    }

    public function updateEquipment(Request $request)
    {
        return app(UpdateEquipmentService::class)
            ->setPayload($request->all())
            ->exec();
    }

    public function getAllEquipment()
    {
        return app(GetAllEquipmentService::class)
            ->exec();
    }
}
