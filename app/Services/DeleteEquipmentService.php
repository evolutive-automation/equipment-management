<?php

namespace App\Services;

use Exception;
use App\Repositories\EquipmentRepository;
use Illuminate\Validation\ValidationException;

class DeleteEquipmentService extends Service
{

    protected $equipmentRepository;
    protected $payload;

    public function __construct(EquipmentRepository $equipmentRepository)
    {
        $this->equipmentRepository = $equipmentRepository;
    }

    public function setPayload($payload)
    {
        $this->payload = $payload;
        return $this;
    }

    private function validateRule()
    {
        $this->validate(
            $this->payload,
            ['id' => 'required|string'],
            ['id.required' => 'column id cant be null']
        );
    }

    public function exec()
    {
        $this->validateRule();

        try {

            $equipment = $this->equipmentRepository->search([
                'id' => $this->payload['id'],
            ]);

            if (count($equipment) == 0) {
                return ok([
                    'ok' => false,
                    'error messages' => '該設備不存在'
                ]);
            }

            $equipment = $this->equipmentRepository->delete($this->payload['id']);

            return ok([
                'ok' => true
            ]);
        } catch (Exception $e) {
            throw ValidationException::withMessages(['error messages' => $e->getMessage()]);
        }
    }
}
