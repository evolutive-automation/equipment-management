<?php

namespace App\Services;

use Exception;
use App\Repositories\EquipmentRepository;
use Illuminate\Validation\ValidationException;

class GetAllEquipmentService extends Service
{

    protected $equipmentRepository;
    protected $payload;

    public function __construct(EquipmentRepository $equipmentRepository)
    {
        $this->equipmentRepository = $equipmentRepository;
    }

    public function exec()
    {
        try {
            $equipment = $this->equipmentRepository->getAllEquipment();

            return ok([
                'ok' => true,
                'data' => $equipment
            ]);
        } catch (Exception $e) {
            throw ValidationException::withMessages(['error messages' => $e->getMessage()]);
        }
    }
}
